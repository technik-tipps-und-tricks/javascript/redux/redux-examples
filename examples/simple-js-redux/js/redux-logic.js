/* Reducer - related to the action decides what to do with the state */
function counter(state, action) {
  if (typeof state === 'undefined') {
      return 0
  }

  switch (action.type) {
      case 'INCREMENT':
          return state + 1
      case 'DECREMENT':
          return state - 1
      default:
          return state
  }
}

/* Redux store */
var store = Redux.createStore(counter)

/* Current value element */
var valueEl = document.getElementById('value')

/* Render current value function */
function render() {
  valueEl.innerHTML = store.getState().toString()
}

/* Initial call of the render function  */
render()

/* Add store listener. Call render on value change */
store.subscribe(render)

/* Action - increment store value */
document.getElementById('increment')
      .addEventListener('click', function () {
          store.dispatch({ type: 'INCREMENT' })
      })

/* Action - decrement store value */
document.getElementById('decrement')
      .addEventListener('click', function () {
          store.dispatch({ type: 'DECREMENT' })
      })

