/* action types */
const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
const DECREMENT_COUNTER = 'DECREMENT_COUNTER';


/* action creators */
function incrementCounter() {
  return { type: INCREMENT_COUNTER }
}

function decrementCounter() {
  return { type: DECREMENT_COUNTER }
}

/* bound action creators */
const boundIncrementCounter = () => dispatch(incrementCounter());
const boundDecrementCounter = () => dispatch(decrementCounter());
