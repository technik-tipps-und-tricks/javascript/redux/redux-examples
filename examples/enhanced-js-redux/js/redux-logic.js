//import { INCREMENT_COUNTER, DECREMENT_COUNTER, incrementCounter, decrementCounter } from './actions.js';
//import { counter } from './reducers';


/* Redux store */
const store = Redux.createStore(counter)

/* Current value element */
const valueEl = document.getElementById('value')

/* Render current value function */
function render() {
  valueEl.innerHTML = store.getState().toString()
  //valueEl.innerHTML = "X"
  //return 'XXX';
}

/* Initial call of the render function  */
render()

/* Add store listener. Call render on value change */
store.subscribe(render)

/* Action - increment store value */
document.getElementById('increment')
      .addEventListener('click', function () {
          //store.dispatch({ type: INCREMENT_COUNTER })
          store.dispatch(incrementCounter());
          //valueEl.innerHTML = 'X'
          //console.log('Yes')
      })

/* Action - decrement store value */
document.getElementById('decrement')
      .addEventListener('click', function () {
          //store.dispatch({ type: DECREMENT_COUNTER })
          store.dispatch(decrementCounter());
      })

      